package com.synthesis.corejava;

public class LearnClassObject {
	
	int id = 0;
	String desc = null;
	
	public static void main(String[] args) {
		
		LearnClassObject obj = new LearnClassObject();
		System.out.println(obj.id);
		System.out.println(obj.desc);
		
		LearnClassObject obj1 = new LearnClassObject();
		obj1.id = 2;
		obj1.desc = "Class and Object";
		System.out.println(obj1.id);
		System.out.println(obj1.desc);
		
		
		//System.out.println("Hello World");
	}

}