package com.synthesis.corejava;

public class LearnStaticKeyword {
	
//
//	static int b = 10;
//	int a = 10;
//	public static void main(String[] args) {
//		
//		LearnStaticKeyword ls = new LearnStaticKeyword();
//		System.out.println(ls.b);
//		System.out.println(ls.a);
//		ls.b++;
//		ls.a++;
//		LearnStaticKeyword ls2 = new LearnStaticKeyword();
//		System.out.println(ls2.b);
//		System.out.println(ls2.a);
//	}
	
	
	 private static int numInstances = 0;

	   protected static int getCount() {
	      return numInstances;
	   }

	   private static void addInstance() {
	      numInstances++;
	   }

	   LearnStaticKeyword() {
		   LearnStaticKeyword.addInstance();
	   }

	   public static void main(String[] arguments) {
	      System.out.println("Starting with " + LearnStaticKeyword.getCount() + " instances");

	      for (int i = 0; i < 500; ++i) {
	         new LearnStaticKeyword();
	      }
	      System.out.println("Created " + LearnStaticKeyword.getCount() + " instances");
	   }
	
}
