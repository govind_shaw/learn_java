package com.synthesis.corejava;

public class LearnControlStatement {

	public static void main(String[] args) {
		int a=10;
		int b=20;
		//if - elseif statment
		if (a > b) {
			System.out.println("A is greater than B.");
		}else if(b > a){
			System.out.println("B is greater than A.");
		}else {
			System.out.println("Both are equals.");
		}
		
		//Switch statement
		int day = 2;
		String dayName = "";
		switch(day) {
		case 0: dayName = "Sunday";
		break;
		case 1: dayName = "Monday";
		break;
		case 2: dayName = "Tuesday";
		break;
		case 3: dayName = "Wednesday";
		break;
		case 4: dayName = "Thursday";
		break;
		case 5: dayName = "Friday";
		break;
		case 6: dayName = "Saturday";
		break;
		default:
			System.out.println("Invalid Day.");
		}
		System.out.println("Day is " + dayName);
		
		int i=10;
		
		for( int j=0; j<=i; j++) {
			System.out.print(j);
		}
		
		//Even numbers between 0-10
		for (int j=0; j <=i; j++) {
			if (j%2 == 0) {
				System.out.println(j);
			}
		}
		 int[] arr = {21,22,23,24,25};
		 for(int e:arr) {
			 System.out.println(e);
			 
		 }
	}

}
