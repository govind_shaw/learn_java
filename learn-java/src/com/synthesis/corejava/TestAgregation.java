package com.synthesis.corejava;

public class TestAgregation {
	public static void main(String args[]){  
		Student s1=new Student(111,"ankit",5000f, new Address("Kormanga", "Bangalore"));  
		Student s2=new Student(112,"sumit",6000f, new Address("Bellandur", "Bangalore"));  
		s1.display();  
		s2.display();  
	}

}
