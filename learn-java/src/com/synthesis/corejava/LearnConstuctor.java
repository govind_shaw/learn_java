package com.synthesis.corejava;

public class LearnConstuctor {

	int id;  
	String name;
	String details;
	//method to display the value of id and name
	//default constructor will be hidden if there is no parameterized constructor
	
	
	LearnConstuctor(){};
	
	//Parametrized constructor
	LearnConstuctor(int theId, String theName) {
		id = theId;
		name = theName;
	}
	
	LearnConstuctor(int i,String n,String a){  
	    id = i;  
	    name = n;  
	    details=a;  
	    }  
	  
	void display(){System.out.println(id+" "+name);} 
	
	void details(){System.out.println(id+" "+name+ " "+details);}
	
	public static void main(String args[]){  
		//creating objects  
		LearnConstuctor s1=new LearnConstuctor();  
		LearnConstuctor s2=new LearnConstuctor();  
		//displaying values of the object  
		s1.display();  
		s2.display();
		
		
		//call paramaterized constuctor
		
		LearnConstuctor s3=new LearnConstuctor(1, "Govind");  
		LearnConstuctor s4=new LearnConstuctor(2, "Ravi");  
		//displaying values of the object  
		s3.display();  
		s4.display();
		
		//Constructor overloading
		LearnConstuctor s5=new LearnConstuctor(1, "Govind", "Developer");  
		LearnConstuctor s6=new LearnConstuctor(2, "Ravi", "Developer");  
		//displaying values of the object  
		s5.details();  
		s6.details();
	}  
}
