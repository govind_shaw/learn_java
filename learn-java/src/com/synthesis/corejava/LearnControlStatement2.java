package com.synthesis.corejava;

public class LearnControlStatement2 {

	public static void main(String[] args) {
		
		int i = 5;
		while(i > 0) {
			System.out.println(i);
			i--;
		}
		
		System.out.println("--------------------------");
		
		int j = 5;
		do {
			System.out.println(j);
			j--;
		}while(j > 1);
		
		System.out.println("--------------------------");
		
		do {
			System.out.println(j);
		}while(j > 10);
		
//		System.out.println("-------infinite------------------");
//		
//		do {
//			System.out.println(j);
//		}while(true);
		
		System.out.println("--------------break---------------");
		
		for(int a=1;a<=6;a++){  
	        if(a==2){
	            break;  
	        }  
	        System.out.println(a);  
	    } 
		
		System.out.println("--------------continue---------------");
		
		for(int a=1;a<=6;a++){  
	        if(a==2){
	        	continue;  
	        }  
	        System.out.println(a);  
	    }  
	}

}
