package com.synthesis.corejava;

public class LearnVariableDataTypes {
	int var1 = 10; //Instance Variable
	static int var2 = 20; //static variable 
	public static void main(String[] args) {
		int var3 = 30; //Local variable
		System.out.println(var2);
		System.out.println(var3);
		LearnVariableDataTypes obj = new LearnVariableDataTypes();
		obj.nonstaticMethod();
	}
	
	public void nonstaticMethod(){
		System.out.println(var1);
	}

}
